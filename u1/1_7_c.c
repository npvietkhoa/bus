#include <stddef.h>
#include <stdio.h>

void exchange(int *i1, int *i2, int  *max) {

    int tmp;
    if(*i1 > *i2)
    {
        if (*i1 > *max)
            *max = *i1;
    }
    else
    {
        if(*i2 > *max)
            *max = *i2;
        tmp = *i1;
        *i1 = *i2;
        *i2 = tmp;
    }
}

int main()
{
    int max = 0;
    int array[10] = {4, 6, 2, 0, 9, 1, 5, 7, 8, 3};
    size_t anz = sizeof(array)/sizeof(*array);
    
    for(size_t i = 0; i < anz; ++i)
    {
        for(size_t j = i + 1; j < anz; ++j)
            exchange(&array[i], &array[j], &max);
    }

    printf("Die Zahlen in sortierter Reihenfolge:");
    for(size_t i = 0; i < anz; ++i) {
        printf(" %d", array[i]);
    }

    printf("\nDas Maximum: %d\n", max);
    return 0;
}
