#!/bin/bash

# 3-er Gruppen
cat teamnamen.txt | grep -v "^$" | sort | uniq -D | uniq -c | awk '/ *3 /' | wc -l

# 2-er Gruppen
cat teamnamen.txt | grep -v "^$" | sort | uniq -D | uniq -c | awk '/ *2 /' | wc -l

# 1-er Gruppen
cat teamnamen.txt | grep -v "^$" | sort | uniq -u | wc -l

# keinen Teamnamen
cat teamnamen.txt | grep -e "^$" | wc -l
