#!/bin/bash
ls -sha1 | sed 's/^ *//g' | sed -e 's/\(\S*\) *\(\S*\)/\2 \1 /g'
