#include <stdio.h>

int main()
{
    double k = (double) 9/5;                           
    double celsius;                             
    double fahrenheit;
    for(celsius = 0; celsius <= 20; ) {
      fahrenheit = (k * celsius) + 32;
      printf("%6.2lf | %6.2lf\n", celsius, fahrenheit);
      celsius += 2;
    }
}
