#!/bin/bash
(
  echo -n "Nullergruppen " ;
  cat teamnamen.txt | grep -e "^$" | wc -l;
  echo -n "Einergruppen ";
  cat teamnamen.txt | grep -v "^$" | sort | uniq -u | wc -l;
  echo -n "Zweiergruppen " ;
  cat teamnamen.txt | grep -v "^$" | sort | uniq -D | uniq -c | awk '/ *2 /' | wc -l;
  echo -n "Dreiergruppen " ;
  cat teamnamen.txt | grep -v "^$" | sort | uniq -D | uniq -c | awk '/ *3 /' | wc -l; 
) | sort -k 2 -n

